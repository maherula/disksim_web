#!/usr/bin/env python3

from bottle import run, get, post, request, route, static_file
import subprocess
import os
import sys


SCRIPT_PATH = os.path.dirname(os.path.realpath(sys.argv[0])) + '/'


@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root=SCRIPT_PATH + 'static/')


@get('/')
def generator():
    return server_static("index.html")


@post('/')
def generate():
    # Collect parameters from forms
    raid_type = request.forms.get('raid_type')
    disks_num = request.forms.get('disks_num')
    io_req = request.forms.get('io_req')
    seq_prob = float(request.forms.get('seq_prob')) / 100
    r_prob = float(request.forms.get('r_prob')) / 100
    proc_n = request.forms.get('proc_n')
    metric_type = request.forms.get('metric_type')

    # Run script to generate metrics
    params1 = ['./run_raid_procs.sh', '-p' + proc_n, '-r' + io_req, '-S' +
               str(seq_prob), '-R' + str(r_prob),
               ''.join(raid_type.lower().split()), disks_num]
    subprocess.call(params1, cwd=SCRIPT_PATH + 'disksim/script')

    # Run script to collect generated metrics
    params2 = ['./run_raid_procs.sh', '--ascii', metric_type]
    sub_res = subprocess.check_output(params2,
                                      cwd=SCRIPT_PATH + 'disksim/script')

    # Format script output to JS array format
    res_str = '['
    i = 1
    for line in sub_res.splitlines():
        s_line = line.split()
        if i > 1:
            res_str += ','
        res_str += '[{},{}]'.format(i, s_line[2].decode("utf-8"))
        i += 1
    res_str += ']'

    # Return JS array
    return res_str

run(host='localhost', port=8070)
