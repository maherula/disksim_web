$(document).ready(function() {

    $("#io_req_slider").slider({
        ticks: [250, 500, 1000, 2500, 5000, 10000],
        ticks_positions: [0, 8, 16, 32, 64, 100],
        ticks_labels: [250, 500, 1000, 2500, 5000, 10000],
        ticks_snap_bounds: 30,
        step: 250,
        value: 1000,
        tooltip: 'always',
        formatter: function(value) {
            return value + ' requests';
        }
    });

    $("#disks_slider").slider({
        id: "disks_slider",
        tooltip: 'always',
        tooltip_position: 'up',
        formatter: function(value) {
            return value + ' disks';
        }
    });

    $("#seq_prob_slider").slider({
        id: "seq_prob_slider",
        tooltip: 'always',
        tooltip_position: 'up',
        formatter: function(value) {
            return value + '% of seq';
        }
    });

    $("#r_prob_slider").slider({
        id: "r_prob_slider",
        tooltip: 'always',
        tooltip_position: 'up',
        formatter: function(value) {
            return value + '% of read';
        }
    });

    $("#proc_n_slider").slider({
        id: "proc_n_slider",
        tooltip: 'always',
        tooltip_position: 'up',
        formatter: function(value) {
            return value + ' processes';
        }
    });

    $("#first_form").submit(function(event) {
        event.preventDefault();

        // Recive metrics from server
        var posting = $.post("/", $("#first_form").serialize());

        // Handle recived data
        posting.done(function(data) {

            var plot_data = $.parseJSON(data);

            var options = {
                grid: { borderColor: '#ccc' },
                series: { shadowSize: 0, color: "#33ff33" },
                xaxis: { show: true }
            };

            // Change plot header to metric name
            $("#result").empty().append('<h2>' + $("#metric_menu").val() + '</h2>');

            // Draw plot
            $.plot("#chart1", [plot_data], options);
        });


        return false
    });


});
