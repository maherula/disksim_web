# Disksim web

## Install requirements
### This project
This project requires installed Python3 with framework [Bottle](http://bottlepy.org/docs/dev/) and script from repo [jakubkam/disksim](https://gitlab.fit.cvut.cz/jakubkam/disksim).

```
$ sudo apt-get install python3 python3-pip
$ sudo pip install bottle
```

### Disksim
Disksim requires installed C development tools, `bison` (YACC-compatible parser generator) and `flex` (fast lexical analyzer generator).

The example of installation command for Ubuntu distribution:
```
$ sudo apt-get install build-essential bison flex
```

## Installation
### Clone this repo
```
$ git@gitlab.fit.cvut.cz:maherula/disksim_web.git
$ cd disksim_web
```

### Clone "disksim builder and scripts" repo
```
$ git clone https://gitlab.fit.cvut.cz/jakubkam/disksim
$ cd disksim
```

### Compile disksim
Require `gcc`, `bison` and `flex`
```
$ make
```
### Add permissions
```
$ cd ..
$ chmod u+x ./server.py
```

## Run
Require `python3` and `bottle`
```
$ ./server.py
```
Server will be started on `localhost` with port `8070`, you can open it with this link [http://localhost:8070/](http://localhost:8070/). To change ip and port, change last line in file `./server.py`.